from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms import ValidationError
from wtforms.fields import (
    BooleanField,
    IntegerField,
    PasswordField,
    StringField,
    SubmitField,
)
from flask_wtf.recaptcha import RecaptchaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length

from app.models import User


class LoginForm(FlaskForm):
    email = EmailField('Email address',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    password = PasswordField('Password', validators=[InputRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit1 = SubmitField('Login')


class RegistrationForm(FlaskForm):
    first_name = StringField('First name',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    surname = StringField('Surname',
                          validators=[InputRequired(),
                                      Length(1, 64)])
    id_number = StringField('ID Number/Passport Number',
                            validators=[InputRequired(),
                                        Length(1, 64)])
    email = EmailField('Email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 EqualTo('password2', 'Passwords must match')
                             ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])
    recaptcha = RecaptchaField()
    submit2 = SubmitField('Register')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class PublisherRegistrationForm(FlaskForm):
    first_name = StringField('First name',
                             validators=[InputRequired(),
                                         Length(1, 64)])
    email = EmailField('Email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 EqualTo('password2', 'Passwords must match')
                             ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])
    recaptcha = RecaptchaField()
    submit = SubmitField('Register')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered. (Did you mean to '
                                  '<a href="{}">log in</a> instead?)'.format(
                url_for('account.index')))


class RequestResetPasswordForm(FlaskForm):
    email = EmailField('Email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    submit = SubmitField('Reset password')

    # We don't validate the email address so we don't confirm to attackers
    # that an account with the given email exists.


class ResetPasswordForm(FlaskForm):
    email = EmailField('Email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    new_password = PasswordField('New password',
                                 validators=[
                                     InputRequired(),
                                     EqualTo('new_password2',
                                             'Passwords must match.')
                                 ])
    new_password2 = PasswordField('Confirm new password',
                                  validators=[InputRequired()])
    submit = SubmitField('Reset password')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() is None:
            raise ValidationError('Unknown email address.')


class CreatePasswordForm(FlaskForm):
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 EqualTo('password2', 'Passwords must match.')
                             ])
    password2 = PasswordField('Confirm new password',
                              validators=[InputRequired()])
    submit = SubmitField('Change password')


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Old password', validators=[InputRequired()])
    new_password = PasswordField('New password',
                                 validators=[
                                     InputRequired(),
                                     EqualTo('new_password2',
                                             'Passwords must match.')
                                 ])
    new_password2 = PasswordField('Confirm new password',
                                  validators=[InputRequired()])
    submit = SubmitField('Update password')


class ChangeEmailForm(FlaskForm):
    email = EmailField('New email',
                       validators=[InputRequired(),
                                   Length(1, 64),
                                   Email()])
    password = PasswordField('Password', validators=[InputRequired()])
    submit = SubmitField('Update email')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')
