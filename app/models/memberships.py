from datetime import datetime

from .. import db


class Membership(db.Model):
    __tablename__ = 'memberships'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_joined = db.Column(db.String(64), index=True)
    name_of_membership = db.Column(db.String(256), index=True)
    membership_registration_number = db.Column(db.String(64))

    def __repr__(self):
        return '<Profession\'%s\'>' % self.name_of_membership

