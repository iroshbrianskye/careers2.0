from datetime import datetime

from .. import db


class ManualEntry(db.Model):
    __tablename__ = 'manual_entries'
    job_reference_number = db.Column(db.String(300))
    job_title = db.Column(db.String(300))
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64), index=True)
    surname = db.Column(db.String(64), index=True)
    second_name = db.Column(db.String(100))
    gender = db.Column(db.String(64))
    id_number = db.Column(db.String(100))
    email = db.Column(db.String(64), index=True)
    phone_number_personal = db.Column(db.String(100), index=True)
    password_hash = db.Column(db.String(128))
    address = db.Column(db.String(100))
    age = db.Column(db.String(100))
    home_county = db.Column(db.String(100))
    ethnicity = db.Column(db.String(64))
    phone_number_alternate = db.Column(db.String(100))
    title = db.Column(db.String(64), index=True)
    civil_status = db.Column(db.String(64))
    nationality = db.Column(db.String(100))
    country_of_residence = db.Column(db.String(256))
    area_of_residence = db.Column(db.String(256))
    disability = db.Column(db.Boolean, default=False)
    disability_nature = db.Column(db.String(120))
    disability_registration_details = db.Column(db.String(120))
    active = db.Column(db.Boolean, default=True)
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    county = db.Column(db.String(100))
    highest_level_education = db.Column(db.String(64))
    exact_degree_diploma_title = db.Column(db.String(100))
    phd_certificate_obtained = db.Column(db.String(256), index=True)
    masters_certificate_obtained = db.Column(db.String(256), index=True)
    degree_certificate_obtained = db.Column(db.String(256), index=True)
    diploma_certificate_obtained = db.Column(db.String(256), index=True)
    higher_diploma_certificate_obtained = db.Column(db.String(256), index=True)
    certificate_obtained = db.Column(db.String(256), index=True)
    relevant_certification = db.Column(db.String(256), index=True)
    registration = db.Column(db.String(256), index=True)
    total_work_experience = db.Column(db.Float, index=True)
    relevant_work_experience = db.Column(db.Float, index=True)
    mode_of_application = db.Column(db.String(256), index=True)
    data_entered_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    job_listing_id = db.Column(db.Integer, db.ForeignKey('job_listings.id'))
    date_entered = db.Column(db.DateTime(), default=datetime.utcnow)
    cv = db.Column(db.Boolean, default=False)
    cover_letter = db.Column(db.Boolean, default=False)
    application_form = db.Column(db.Boolean, default=False)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<ManualEntry\'%s\'>' % self.first_name

    def get_time(self):
        return self.createdAt


