from .. import db
from datetime import datetime


class Applicant(db.Model):
    __tablename__ = 'applicants'
    id = db.Column(db.Integer, primary_key=True)
    overview = db.Column(db.String(2000))
    facebook = db.Column(db.String(120))
    twitter = db.Column(db.String(120))
    instagram = db.Column(db.String(120))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Applicant\'%s\'>' % self.user_id
