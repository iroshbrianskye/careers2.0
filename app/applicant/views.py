import uuid

from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory)
from flask_ckeditor import upload_success, upload_fail
from flask_login import current_user, login_required
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from flask_uploads import patch_request_class

from app.models.job_listings import *
from app.models.applications import *
from app.models.education_documents import *
from app.models.users import *
from app.applicant.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import User, Listing
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from app.decorators import checks_required
from werkzeug.utils import secure_filename
from app.lib.upload_file import uploadfile
from app.auth.email import send_applicant_submitted
from werkzeug.datastructures import FileStorage, MultiDict
from datetime import datetime
from datetime import date
import africastalking

username = "kutrrh"
api_key = "316dda421e1e2ee09e95d218ee5b7cdddef60b2e4f3f129dd8f2537c64a271f2"
africastalking.initialize(username, api_key)

sms = africastalking.SMS

ALLOWED_EXTENSIONS = {
    ".pdf", ".PDF", '.txt', '.png', '.jpg', '.jpeg', '.gif', ".png", ".jpg", ".jpeg",
    ".docx"
}
MAX_CONTENT_LENGTH = 10 * 1024 * 1024
applicant = Blueprint("applicant", __name__)
certificates = UploadSet("certificates", ['pdf'])


@applicant.route("/")
@login_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""
    now = datetime.now()
    today = date.today()
    deadline = now.strftime("%d/%m/%Y")
    today_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)
    cancelled = current_user.applications.filter_by(status="cancelled").count()
    accepted = current_user.applications.filter_by(status="accepted").count()
    successful_applications = (
        current_user.applications.filter_by(status="applied").order_by(Application.createdAt.desc()).all()
    )
    all_applications = (
        current_user.applications.order_by(Application.createdAt.desc()).all()
    )
    all_jobs = Listing.query.filter_by(published=True).order_by(Listing.createdAt.desc()).all()

    valid_jobs = Listing.query.filter_by(published=True).filter(
        Listing.availability_to >= today_datetime).order_by(Listing.createdAt.desc()).all()

    return render_template(
        "applicant/index.html",
        cancelled=cancelled,
        accepted=accepted,
        all_applications=all_applications,
        successful_applications=successful_applications,
        all_jobs=all_jobs,
        valid_jobs=valid_jobs,
        now=now,
        deadline=deadline
    )


@applicant.route("/new_application", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def new_application():
    """Start a new application."""
    form = SelectJob()
    job_title = form.job.data
    if form.validate_on_submit():
        return redirect(url_for("applicant.start_application", job_title=job_title.job_title))
    return render_template("applicant/select_job.html", form=form)


@applicant.route("/start_application/<job_title>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def start_application(job_title):
    """Apply."""
    form = StartAppForm()
    job = Listing.query.filter_by(job_title=job_title).first_or_404()
    application = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == job.id).first()
    if form.validate_on_submit():
        if application:
            if application.status == 'applied':
                flash("You have applied for {} already boss!".format(job_title), "danger")
                return redirect(url_for("applicant.dashboard"))
        else:
            start_application = Application(
                job_listing_id=job.id,
                user_id=current_user.id
            )
            db.session.add(start_application)
            db.session.commit()
        return redirect(url_for("applicant.edit_application", id=job.id))
    return render_template("applicant/start_application.html", job=job, form=form)


@applicant.route("/apply_job/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def edit_application(id):
    user = current_user
    job_applied = Listing.query.filter_by(id=id).first()
    application_to_complete = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == id).first_or_404()
    this_ed = EducationDocuments.query.filter_by(user_id=user.id).first()
    form = ApplicationDocuments()
    if form.validate_on_submit():
        application_to_complete.agreement = form.terms_and_conditions.data
        docs = EducationDocuments(
            cv_url=str(user.id) + str(application_to_complete.id) + 'cv' + str(current_user.id_number) + '.pdf',
            cover_letter_url=str(user.id) + str(application_to_complete.id) + 'cl' + str(
                current_user.id_number) + '.pdf',
            application_form_url=str(user.id) + str(application_to_complete.id) + 'ap' + str(
                current_user.id_number) + '.pdf',
            user_id=current_user.id,
            application_id=application_to_complete.id,
            years_of_experience=form.years_of_experience.data,
            license=str(user.id) + str(application_to_complete.id) + 'l' + str(current_user.id_number) + '.pdf'
        )
        days = DaysAvailable(
            application_id=application_to_complete.id,
            monday=form.monday.data,
            tuesday=form.tuesday.data,
            wednesday=form.wednesday.data,
            thursday=form.thursday.data,
            friday=form.friday.data,
            saturday=form.saturday.data,
            sunday=form.sunday.data,
        )
        db.session.add(docs)
        db.session.add(days)
        db.session.commit()
        return redirect(url_for('applicant.submit_application', id=application_to_complete.id))
    return render_template("applicant/apply.html", job_applied=job_applied,
                           application_to_complete=application_to_complete, form=form)


@applicant.route("/start_edit_application/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def start_edit_application(id):
    """Apply."""
    form = EditStartAppForm()
    job = Listing.query.filter_by(id=id).first_or_404()
    application = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == job.id).first()
    if form.validate_on_submit():
        return redirect(url_for("applicant.edit_job_application", id=application.id))
    return render_template("applicant/start_edit_application.html", job=job, form=form)


@applicant.route("/edit_application/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def edit_job_application(id):
    """Edit Application page."""
    application_to_edit = Application.query.filter_by(id=id).first_or_404()
    this_ed = EducationDocuments.query.filter_by(user_id=current_user.id, application_id=id).first()
    job_id = application_to_edit.job_listing_id
    job_applied = Listing.query.filter_by(id=job_id).first()
    form = EditApplicationDocuments(obj=this_ed)
    if form.validate_on_submit():
        application_to_edit.status = 'applied'
        if this_ed:
            form.populate_obj(this_ed)
        else:
            new_docs = EducationDocuments(
                cv_url=str(current_user.id) + str(application_to_edit.id) + 'cv' + str(current_user.id_number) + '.pdf',
                cover_letter_url=str(current_user.id) + str(application_to_edit.id) + 'cl' + str(
                    current_user.id_number) + '.pdf',
                application_form_url=str(current_user.id) + str(application_to_edit.id) + 'ap' + str(
                    current_user.id_number) + '.pdf',
                user_id=current_user.id,
                application_id=application_to_edit.id,
                years_of_experience=form.years_of_experience.data,
                license=str(current_user.id) + str(application_to_edit.id) + 'l' + str(current_user.id_number) + '.pdf'
            )
            db.session.add(new_docs)
        db.session.commit()
        flash("Successfully updated.", "success")
        return redirect(url_for('applicant.submit_application', id=application_to_edit.id))
    return render_template("applicant/edit_job_app.html",
                           application_to_edit=application_to_edit, form=form, this_ed=this_ed,
                           job_applied=job_applied)


@applicant.route("/submit_final/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def submit_application(id):
    this_ed = EducationDocuments.query.filter_by(user_id=current_user.id).first()
    this_applicant = Applicant.query.filter_by(user_id=current_user.id).first()
    user = User.query.filter_by(id=current_user.id).first_or_404()
    final_application = Application.query.filter_by(id=id).first()
    final_application.complete_profile = True
    final_application.status = "applied"
    if this_applicant is None:
        applicant = Applicant(
            user_id=current_user.id
        )
        db.session.add(applicant)
        db.session.commit()
    send_applicant_submitted(user)
    flash("Successfully applied for this vacancy.", "success")
    return redirect(url_for("applicant.view_applications"))


@applicant.route("/personal_history", methods=["post", "get"])
@login_required
@check_confirmed
def personal_history():
    """Personal History page."""
    form = PersonalDetailsForm(obj=current_user)
    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        flash("Personal Details updated successfully", "success")
        return redirect(url_for("applicant.contact_info"))
    return render_template("applicant/personal_details.html", form=form)


@applicant.route("/check_personal_history")
@login_required
@check_confirmed
def check_personal_history():
    if current_user.first_name is not None and current_user.surname is not None and \
            current_user.second_name is not None and current_user.gender is not None and \
            current_user.home_county is not None and current_user.dob is not None and current_user.civil_status is not None and \
            current_user.nationality is not None and current_user.disability is not None and current_user.ethnicity is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/contact_info", methods=["post", "get"])
@login_required
@check_confirmed
def contact_info():
    """Contact Info page."""
    form = ContactInfoForm(obj=current_user)
    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        flash("Contact Details updated successfully", "success")
        return redirect(url_for("applicant.education_info"))
    return render_template("applicant/contact_info.html", form=form)


@applicant.route("/check_contact_info")
@login_required
@check_confirmed
def check_contact_info():
    if current_user.email is not None and current_user.address is not None and \
            current_user.phone_number_personal is not None and current_user.area_of_residence is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/education_information", methods=["post", "get"])
@login_required
@check_confirmed
def education_info():
    """Education Details page."""
    user = User.query.filter_by(id=current_user.id).first()
    this_ed = EducationDocuments.query.filter_by(user_id=current_user.id).first()
    form = EducationDetailsForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        flash("Academic Details updated successfully.", "success")
        return redirect(url_for("applicant.employment_history"))
    return render_template("applicant/education_info.html", form=form, this_ed=this_ed)


@applicant.route("/check_education_info")
@login_required
@check_confirmed
def check_education_info():
    phd = Phd.query.filter_by(user=current_user).first()
    masters = Masters.query.filter_by(user=current_user).first()
    degree = Degree.query.filter_by(user=current_user).first()
    higher_diploma = HigherDiploma.query.filter_by(user=current_user).first()
    diploma = Diploma.query.filter_by(user=current_user).first()
    certificate = Certificate.query.filter_by(user=current_user).first()

    if phd is not None and masters is not None and degree is not None and higher_diploma is not None \
            and diploma is not None and certificate is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/employment_information", methods=["post", "get"])
@login_required
@check_confirmed
def employment_history():
    """Employment History page."""
    this_eh = Employment.query.filter_by(user_id=current_user.id).first()
    form2 = EmpForm(obj=this_eh)
    if form2.validate_on_submit():
        if this_eh:
            form2.populate_obj(this_eh)
        else:
            eh = Employment(
                user_id=current_user.id,
                if_not_employed=form2.if_not_employed.data,
                ministry=form2.ministry.data,
                station=form2.station.data,
                pf_number=form2.pf_number.data,
                personal_employment_no=form2.personal_employment_no.data,
                present_substantive_post=form2.present_substantive_post.data,
                job_group=form2.job_group.data,
                date_of_current_appointment=form2.date_of_current_appointment.data,
                terms_of_service=form2.terms_of_service.data,
                if_other=form2.if_other.data,
                gross_salary=form2.gross_salary.data,
                current_employer=form2.current_employer.data,
                effective_date=form2.effective_date.data,
                position_held=form2.position_held.data,
            )
            db.session.add(eh)
        db.session.commit()
        flash("Employment History updated successfully. Fill in Year by Year Details if applicable.", "success")
        return redirect(url_for("applicant.employment_history_by_details"))
    return render_template("applicant/employment_history.html", form2=form2, this_eh=this_eh)


@applicant.route("/check_employment_history")
@login_required
@check_confirmed
def check_employment_history():
    employment = Employment.query.filter_by(user=current_user).first()
    if employment is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/_employment_history", methods=["post", "get"])
@login_required
@check_confirmed
def employment_history_by_details():
    user = User.query.filter_by(id=current_user.id).first()
    form = EmploymentForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        flash("Employment History updated successfully.", "success")
        return redirect(url_for("applicant.family_details"))
    return render_template("applicant/_employment_history.html", form=form)


@applicant.route("/check_employment_history_by_details")
@login_required
@check_confirmed
def check_employment_history_by_details():
    employment_history = EmploymentHistory.query.filter_by(user=current_user).first()
    if employment_history is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/family_details", methods=["post", "get"])
@login_required
@check_confirmed
def family_details():
    """Family Details page."""
    this_family = Family.query.filter_by(user_id=current_user.id).first()
    form = FamilyForm(obj=this_family)
    if form.validate_on_submit():
        if this_family:
            form.populate_obj(this_family)
        else:
            family_det = Family(
                user_id=current_user.id,
                name=form.name.data,
                gender=form.gender.data,
                relationship=form.relationship.data,
                phone_number=form.phone_number.data
            )
            db.session.add(family_det)
        db.session.commit()
        flash("Family Details updated successfully", "success")
        return redirect(url_for("applicant.reference_details"))
    # if this_family:
    #     family_dob = this_family.dob.strftime('%d/%m/%Y')
    return render_template("applicant/family_details.html", form=form, this_family=this_family)


@applicant.route("/check_family_details")
@login_required
@check_confirmed
def check_family_details():
    family_info = Family.query.filter_by(user=current_user).first()
    if family_info is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/references", methods=["post", "get"])
@login_required
@check_confirmed
def reference_details():
    """References page."""
    this_ref = Reference.query.filter_by(user_id=current_user.id).first()
    form = ElForm(obj=this_ref)
    if form.validate_on_submit():
        if this_ref:
            form.populate_obj(this_ref)
        else:
            ref_det = Reference(
                user_id=current_user.id,
                name=form.name.data,
                address=form.address.data,
                phone_number=form.phone_number.data,
                email_address=form.email_address.data,
                occupation=form.occupation.data,
                institution=form.institution.data,
                name_2=form.name_2.data,
                address_2=form.address_2.data,
                phone_number_2=form.phone_number_2.data,
                email_address_2=form.email_address_2.data,
                occupation_2=form.occupation_2.data,
                institution_2=form.institution_2.data,
                name_3=form.name_3.data,
                address_3=form.address_3.data,
                phone_number_3=form.phone_number_3.data,
                email_address_3=form.email_address_3.data,
                occupation_3=form.occupation_3.data,
                institution_3=form.institution_3.data
            )
            db.session.add(ref_det)
        db.session.commit()
        flash("Reference Details updated successfully", "success")
        return redirect(url_for("applicant.profile_summary"))
    return render_template("applicant/reference_details.html", form=form)


@applicant.route("/check_reference_details")
@login_required
@check_confirmed
def check_reference_details():
    references = Reference.query.filter_by(user=current_user).first()
    if references is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/view/application/<status>")
@login_required
@check_confirmed
def view_applications_by(status):
    """Admin dashboard page."""
    my_applications = Application.query.filter_by(
        user=current_user, status=status
    ).all()
    return render_template(
        "applicant/application_by_status.html",
        my_applications=my_applications,
        status=status,
    )


@applicant.route("/view/applications")
@login_required
@check_confirmed
def view_applications():
    """Admin dashboard page."""
    user = current_user
    my_applications = Application.query.filter_by(
        user=current_user
    ).order_by(Application.createdAt.desc()).all()

    my_docs = EducationDocuments
    editable_applications = (
        Application.query.join(Listing, (Listing.id == Application.job_listing_id))
        .filter(Listing.availability_to >= today_datetime)
        .order_by(Application.createdAt.desc())
        .all())

    return render_template(
        "applicant/applications.html", my_applications=my_applications, my_docs=my_docs, user=user,
        editable_applications=editable_applications
    )


@applicant.route("/accept_offer")
@login_required
@check_confirmed
def accept_offer():
    """Admin dashboard page."""
    return redirect(url_for("applicant.dashboard"))


@applicant.route("/decline_offer")
def decline_offer():
    """Admin dashboard page."""
    return render_template("applicant/index.html")


@applicant.route("/profile", methods=["post", "get"])
@login_required
@check_confirmed
def profile():
    """Admin dashboard page."""

    form = ProfileForm(obj=current_user)

    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        return redirect(url_for("applicant.profile"))
    return render_template("applicant/user-profile-page.html", form=form)


@applicant.route("/profile_summary", methods=["post", "get"])
@login_required
@check_confirmed
def profile_summary():
    """Profile Summary."""
    user = current_user
    education_details = Education.query.filter_by(user_id=current_user.id).all()
    phd = Phd.query.filter_by(user_id=current_user.id).all()
    masters = Masters.query.filter_by(user_id=current_user.id).all()
    degree = Degree.query.filter_by(user_id=current_user.id).all()
    higher_diploma = HigherDiploma.query.filter_by(user_id=current_user.id).all()
    diploma = Diploma.query.filter_by(user_id=current_user.id).all()
    certificate = Certificate.query.filter_by(user_id=current_user.id).all()
    employment_details = Employment.query.filter_by(user_id=current_user.id).first_or_404()
    employment_history = EmploymentHistory.query.filter_by(user_id=current_user.id).all()
    family = Family.query.filter_by(user_id=current_user.id).first_or_404()
    referees = Reference.query.filter_by(user_id=current_user.id).first_or_404()
    prof = Professional.query.filter_by(user_id=current_user.id).all()
    return render_template("applicant/profile_summary.html", education_details=education_details,
                           employment_history=employment_history, employment_details=employment_details, family=family,
                           referees=referees, user=user, prof=prof, phd=phd, masters=masters, degree=degree,
                           higher_diploma=higher_diploma, diploma=diploma, certificate=certificate)


@applicant.route("/settings", methods=["post", "get"])
@login_required
@check_confirmed
def settings():
    """Admin dashboard page."""

    if current_user.is_anonymous:
        return redirect(url_for("home.index"))
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.", "success")
            return redirect(url_for("applicant.dashboard"))
        else:
            flash("Original password is invalid.", "warning")
    return render_template("account/reset_password.html", form=form)


@applicant.route("/send_message/<id>", methods=["GET", "POST"])
@login_required
@check_confirmed
def send_message(id):
    user = User.query.filter_by(id=id).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user, body=form.message.data)
        db.session.add(msg)
        user.add_notification("unread_message_count", user.new_messages())
        db.session.commit()
        flash("Your message has been sent.", "success")
        return redirect(url_for("publisher.messages"))
    return render_template(
        "publisher/send_message.html", title="Send Message", form=form, user=user
    )


@applicant.route("/messages")
@login_required
@check_confirmed
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification("unread_message_count", 0)
    db.session.commit()
    page = request.args.get("page", 0, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()
    ).group_by(Message.sender_id)
    return render_template("applicant/messages.html", messages=messages)


@applicant.route("/da_files/<path:filename>")
def uploaded_files(filename):
    path = current_app.config["UPLOAD_FOLDER"]
    return send_from_directory(path, filename)


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@applicant.route("/upload_cv/<app_id>", methods=['GET', 'POST'])
def upload_cv(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'cv' + str(current_user.id_number) + os.path.splitext(filename)[
            1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("CV Needs to be in allowed format!", "danger")
                return "Invalid file", 400
                # Create target directory & all intermediate directories if don't exists
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
        return '', 204


@applicant.route("/upload_cover_letter/<app_id>", methods=['GET', 'POST'])
def upload_cover_letter(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'cl' + str(current_user.id_number) + os.path.splitext(filename)[
            1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("Cover Letter Needs to be in allowed format!", "danger")
                return "Invalid file", 400
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
        return '', 204


@applicant.route("/upload_application_form/<app_id>", methods=['GET', 'POST'])
def upload_application_form(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'ap' + str(current_user.id_number) + os.path.splitext(filename)[
            1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("Application Form Needs to be in allowed format!", "danger")
                return "Invalid file", 400
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
        return '', 204


@applicant.route("/upload_license/<app_id>", methods=['GET', 'POST'])
def upload_license(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'l' + str(current_user.id_number) + os.path.splitext(filename)[
            1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("License Needs to be in allowed format!", "danger")
                return "Invalid file", 400
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
        return '', 204


def allowed_filesize(filesize):
    if int(filesize) <= MAX_CONTENT_LENGTH:
        return True
    else:
        return False


def gen_file_name(filename):
    """
    If file was exist already, rename it and return a new name
    """

    i = 1
    while os.path.exists(os.path.join(current_app.config["UPLOAD_FOLDER"], filename)):
        name, extension = os.path.splitext(filename)
        filename = "%s_%s%s" % (name, str(i), extension)
        i += 1

    return filename


def create_thumbnail(image):
    try:
        base_width = 100
        img = Image.open(os.path.join(current_app.config["UPLOAD_FOLDER"], image))
        w_percent = base_width / float(img.size[0])
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((base_width, h_size), PIL.Image.ANTIALIAS)
        img.save(os.path.join(current_app.config["THUMBNAIL_FOLDER"], image))

        return True

    except:
        print(traceback.format_exc())
        return False


@applicant.route("/upload/<p_table>/<c_table>/<id>", methods=["GET", "POST"])
def upload_image(p_table, c_table, id):
    if request.method == "POST":
        files = request.files["file"]

        if files:
            filename = secure_filename(files.filename)
            filename = gen_file_name(filename)
            mime_type = files.content_type
            p_model = User.get_class_by_tablename(p_table)

            if not allowed_file(files.filename):
                result = uploadfile(
                    name=filename,
                    table=c_table,
                    type=mime_type,
                    size=0,
                    not_allowed_msg="File type not allowed",
                )

            else:
                # save file to disk
                uploaded_file_path = os.path.join(
                    current_app.config["UPLOAD_FOLDER"], filename
                )
                files.save(uploaded_file_path)
                p_model = User.get_class_by_tablename(p_table)
                c_model = User.get_class_by_tablename(c_table)
                parent_table = p_model.query.filter_by(id=id).first_or_404()
                if parent_table.images.count() >= 5:
                    return "You cannot add more than 5 images", 400
                new_image = c_model(image_url=filename, job_listing_id=parent_table.id)
                db.session.add(new_image)
                db.session.commit()

                # create thumbnail after saving
                if mime_type.startswith("image"):
                    create_thumbnail(filename)

                # get file size after saving
                size = os.path.getsize(uploaded_file_path)

                # return json for js call back
                result = uploadfile(
                    name=filename, table=c_table, type=mime_type, size=size
                )

            return simplejson.dumps({"files": [result.get_file()]})

    if request.method == "GET":
        # get all file in ./data directory
        p_model = User.get_class_by_tablename(p_table)
        c_model = User.get_class_by_tablename(c_table)
        parent_table = p_model.query.filter_by(id=id).first_or_404()
        files = [
            f.image_url
            for f in c_model.query.filter_by(job_listing_id=parent_table.id).all()
            if os.path.isfile(
                os.path.join(current_app.config["UPLOAD_FOLDER"], f.image_url)
            )
        ]
        file_display = []

        for f in files:
            size = os.path.getsize(os.path.join(current_app.config["UPLOAD_FOLDER"], f))
            file_saved = uploadfile(name=f, size=size, table=c_table)
            file_display.append(file_saved.get_file())

        return simplejson.dumps({"files": file_display})

    return redirect(url_for("index"))


@applicant.route("/delete_image/<table>/<string:filename>", methods=["DELETE"])
def delete_image(table, filename):
    file_path = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
    file_thumb_path = os.path.join(current_app.config["THUMBNAIL_FOLDER"], filename)
    c_model = User.get_class_by_tablename(table)
    table = c_model.query.filter_by(image_url=filename).first_or_404()
    db.session.delete(table)
    db.session.commit()
    if os.path.exists(file_path):
        try:
            os.remove(file_path)

            if os.path.exists(file_thumb_path):
                os.remove(file_thumb_path)

            return simplejson.dumps({filename: "True"})
        except:
            return simplejson.dumps({filename: "False"})


# serve static files
@applicant.route("/thumbnail/<string:filename>", methods=["GET"])
def get_thumbnail(filename):
    return send_from_directory(
        current_app.config["THUMBNAIL_FOLDER"], filename=filename
    )


@applicant.route("/data/<string:filename>", methods=["GET"])
def get_file(filename):
    return send_from_directory(
        os.path.join(current_app.config["UPLOAD_FOLDER"]), filename=filename
    )


@applicant.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'],
                               filename)
