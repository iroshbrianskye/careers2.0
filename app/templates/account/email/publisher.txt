Dear {{ user.full_name() }},

You have successfully applied!

{{ user.full_name() }} has successfully submitted a job application.

Sincerely,

The {{ config.APP_NAME }} Team

Note: replies to this email address are not monitored. Do not reply
